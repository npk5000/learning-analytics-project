import csv
import sys
from tld import get_tld   #this is the package top extract tld: pip install tld

csv.field_size_limit(sys.maxsize/2) #raise the max field size probably only needed because of huge 4636

csv_file = "ezproxy.csv"

my_reader = csv.reader(open(csv_file)) #in file

outfile = "all_domains.csv"

f  =  open(outfile,"w")  #out file

all_domains = []

counter = 0
for row in my_reader:
	counter += 1
	try:
		f.write(get_tld(row[5])+"\n")
		all_domains.append(get_tld(row[5]))    # the part that strips the domain
	except:
		f.write("An error at row" + str(counter) + "!\n")

f.write("\n")

unique_domains = set(all_domains)

for domain in unique_domains: #Count how many words are in the list
	f.write(domain + " appears: " + str(all_domains.count(domain)) + " times\n")

	
f.close()